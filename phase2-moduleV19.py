'''

Created on Jul 14, 2015

@author: francisco


'''
##LSF ONLY##

#import stuff for recovering the genomes for phase 2
import subprocess
import os.path
import sys
import argparse
from Bio import Entrez
Entrez.email='Your Email here'

snapMkIndex  = '/your/path/to/snap/snap-aligner index'
snapPaired   = '/your/path/to/snap/snap-aligner paired'
snap         = '/your/path/to/snap/snap-aligner '
bedtools     = '/your/path/to/BEDTools/2.17.0/bin/bedtools'
rscript      = '/your/path/to/R/3.2.2/bin/Rscript'
ezvir2_p1    = '/your/path/to/ezvir2/EZVIR2_reports_phase-1.v150232.r'
ezv2_rf      = '/your/path/to/ezvir2/required_files' 
samtools     = '/your/path/to/samtools/samtools'
histograms   = '/your/path/to/ezvir2/EZVIR2-Histogram_v2.py'
genomeDB     = 'GENOMES_NO-ANIMAL-140540.ezv'


# To do 
def get_args():
    parser = argparse.ArgumentParser(description='Automates phase 2 of ezVir by taking the best results from phase 1 and expanding on them.')
    parser.add_argument('-ezIn', dest='EZ', type=str, nargs=1, required=True, help='Results from phase 1')
    parser.add_argument('-cluster', dest='CDHit', type=str, nargs=1, required=True, help='CDHit cluster file')
    parser.add_argument('-fq1',  dest='f1_tfnh', type=str, nargs=1, required=True, help='fastq file name - Read 1')
    parser.add_argument('-fq2',  dest='f2_tfnh', type=str, nargs=1, required=True, help='fastq file name - Read 2')
    parser.add_argument('-dir',  dest='output_dir', type=str, required=True, help='phase 2 main output directory')
    parser.add_argument('-o',  dest='outName', type=str, required=True, help='output directory for reports')
    parser.add_argument('-c',  dest='cpu', type=restricted_cpu, nargs=1, required=True, help='Number of CPUs to use')
    parser.add_argument('-mem',  dest='mem', type=str, nargs=1, required=True, help='Memory (in Gb)')
    parser.add_argument('-name',  dest='name', type=str, nargs=1, required=True, help='filename pertaining to the current analysis')
    parser.add_argument('-log',  dest='log', type=str, nargs=1, required=True, help='path to log')
    parser.add_argument('-rs',  dest='rsum', type=str, nargs=1, required=True, help='read sum file path')
    parser.add_argument('-rn',  dest='rnum', type=str, nargs=1, required=True, help='read number file path')
    #Get command line args
    return  parser.parse_args()

def restricted_cpu(x):
    x = int(x)
    if x < 1 or x > 64:
        raise argparse.ArgumentTypeError("%r not in correct range of CPUs [1, 64]"%(x,))
    return x

#function to extract the needed genomes to be used by SNAP for phase-2
def queryStuff(File,outFile,type):
    query1=File
    finalOut=open(str(outFile),'w')
    queryType=str(type)
    
    printStart='echo Querying for '+ str(queryType) + ' ' + str(File), '...'
    subprocess.call(printStart, shell=True)
    if queryType == 'genBank':
        handle = Entrez.efetch(db="nucleotide", id=query1, rettype="gb", retmode="text")
    if queryType == 'test':
        handle= Entrez.efetch(db='nuccore',id=query1,rettype='gb',retmode='text')
    if queryType == 'fasta':
        handle = Entrez.efetch(db="nucleotide", id=query1, rettype="fasta", retmode="text")
    if queryType == 'taxonomy':
        handle = Entrez.efetch(db="taxonomy", id=query1, rettype="uilist", retmode="text")
    if queryType == 'fna':
        handle = Entrez.efetch(db="nuccore", id=query1, rettype="fasta", retmode="text")
    if queryType == 'faa':
        handle = Entrez.efetch(db="nuccore", id=query1, rettype="fasta_cds_na", retmode="text")
    finalOut.write(handle.read())
    finalOut.close()
    printEnd='echo Query for ' + str(queryType) +' ' +  str(File) + ' complete.'
    subprocess.call(printEnd, shell=True)


def SplitFasta(inFasta):
    mkIndexes=[]
    indexNameList=[]
    for x in open(str(inFasta)):
        scaffID=str(x).strip('\n')
        #print(scaffID)
        if '>' in scaffID:
            #indexName=scaffID.strip('>').split('|')
            indexName=scaffID.strip('>').split(' ')
            #indexName='_'.join((indexName[0],indexName[1],indexName[2],indexName[3]))
            indexName=indexName[0]
            mkIndexes.append('%s %s%s.fasta %s%s.index'%(snapMkIndex, inFasta, indexName, inFasta, indexName))
            indexNameList.append(indexName+'.index')
            indexFile=open(str(inFasta)+str(indexName)+'.fasta','w')
            indexFile.write(x)
            #print(indexName)
        if '>' not in scaffID:
            indexFile.write(x)
    return (mkIndexes,indexNameList)
    
    
def main():
    args         = get_args()
    ezInput      = args.EZ[0]
    clusterFile  = args.CDHit[0]
    f1_tfnh      = os.path.abspath(args.f1_tfnh[0])
    f2_tfnh      = os.path.abspath(args.f2_tfnh[0])
    oezrP2       = args.outName + '/reports/'
    oezvP2       = args.output_dir
    mem          = args.mem[0]
    cpu          = args.cpu[0]
    od           = args.outName +'/log/'
    on           = args.name[0]
    readSum      = args.rsum[0]
    readNum      = args.rnum[0]
    histogramDir = args.outName+'/histograms/'
    histogramp2  = args.outName
   
    
    ##----->>>>> COMMANDS: Phase-2 <<<<<-----##
    # Take the best results from phase 1 and check the cdhit cluster they belong to
    # Then, extract the genomes from that cluster and re-do phase 1 using those genomes
   
    #1 - parse the ezvir Best results file
    #    <name>_BEST-GENOMES.ezv
    #    Example output from BEST-GENOMES.ezv
    #        ID,GN,LN,DC,DCN
    #        JF742759.1,Astrovirus_MLB2_isolate_MLB2-human-Stl-WD0559-2008-_complete_genome.__31-MAY-2012_,Astrovirus,13.86,#CC7A00
    #        <more genomes here> 

    #1.1 ignore the first line and parse every other line, by taking the accession id
    giIDs=[]
    for x in open(ezInput,'r'):
        splitter=x.strip().split(',')
        if 'ID,GN' not in x:
            giIDs.append(splitter[0])
    
    #1.2 compare these IDs against the CDhit clusters (.clstr file) and extract all IDs within said cluster
    
    #put the cluster file in memory
    readCluster=[]
    for x in open(str(clusterFile),'r'):
        readCluster.append(x.strip())
    
    counter=0
    counter1=1
    targetClusters=''
    #for each line in the cluster file...
    for x in readCluster:
        #if the line is bigger than 2 (ie: it's not a "cluster line", which only has the >Cluster and <number> as entries.) 
        if len(x.split()) > 2:
            #split the line and get the gi ID
            a=x.split()[2]
            b=a.strip('>').strip('...')
            #if the gi ID is in the list of best results from phase-1...
            if b.strip('>') in giIDs: 
                #search for the cluster start
                while 'Cluster' not in readCluster[counter-counter1]:
                    counter1=counter1+1
                startCounter=counter-counter1+1
                #starting from the cluster start, while there isn't a new "Cluster" line"...
                while 'Cluster' not in readCluster[startCounter]:
                    #split the line
                    a=readCluster[startCounter].split()
                    b=a[2].strip('>').strip('...')
                    #extract accession number and if that number isn't already present on the target Cluster list, add it to said list.
                    if str(b) not in targetClusters:
                        targetClusters=targetClusters + str(b) + ','
                        #print(b[3])
                    startCounter=startCounter+1
        
        counter=counter+1
        counter1=1
        
#2 - Take the list and run efetch to get the genomes needed to run SNAP for phase 2
    queryStuff(targetClusters,oezvP2,'fasta')
    
#3 - Split the genomes and create the index based on the downloaded genomes
    mkIndexList,indexList=SplitFasta(oezvP2)
    mkIndexList=' ; '.join(mkIndexList)
    
#4 - Run phase 1 again with the new index file:

    ##----->>>>> COMMANDS: Phase-1: Diagnostic <<<<<-----##

    # SNAP mapping to ezVIR database
    # faster mapping settings
    map_opts = '-F a --hp -H 1000000 -h 10 -d 8'
    
    c18=''
    c19=''
    c20=''
    c21='cat '
    for x in indexList:
        # align the unmapped-to-human reads against each viral index
        c18=c18 + snap + 'paired %s %s %s -t 4 %s -o %s.ezv2.bam ; ' %(oezvP2+str(x), f1_tfnh, f2_tfnh, map_opts, oezvP2+str(x))
        
        # sort the mapped reads by position
        c19 = c19 + '%s sort -@ %s %s.ezv2.bam %s.ezv2.sp ; ' %(samtools, cpu, oezvP2+str(x), oezvP2+str(x))
        
        # genome coverage command to create bga 
        c20 = c20 + '%s genomecov -ibam %s.ezv2.sp.bam -bga > %s.ezv2.bga ; ' %(bedtools, oezvP2+str(x), oezvP2+str(x))

        # take note of the names of all generated bga files in order to merge them later on
        c21= c21 + ' %s.ezv2.bga ' %(oezvP2+str(x)) 

    #remove last 2 characters in order to prevent a double ";" in the lsf command
    c18=c18[:-2]
    c19=c19[:-2]
    c20=c20[:-2]
    
    # merge all bga files into one
    c21=c21 + '> %s.ezv2.bga' %(oezvP2)
    
    # SNAP mapping output for report generation
    ezv2_in = '%s.ezv2.bga' %(oezvP2)
    
    # report generation

    c22 = '%s %s -c %s/COLORS.ezv -v %s/%s -i %s -o %s -r %s -n %s' %(rscript, ezvir2_p1, ezv2_rf, ezv2_rf, genomeDB, ezv2_in, oezrP2+on, readSum, readNum)
    
    #5 - Clean-up step: delete index files.  
    cleanup=('rm -rf %s*.index' %(oezvP2))   
    cleanup2=('rm -rf %s*.fasta' %(oezvP2))
    cleanup3=('rm -rf %s %s'%(readNum, readSum))
    mem_MB = int(mem) * 1000
    memf = 'rusage[mem=%s]' %(mem_MB)
    memv=260000000
    
    # set max wall clock time (default is 30 minute jobs) format hh:mm
    time = '179:59'
    
    # build LSF command
    # histogram generation
    c23='python %s -cl ezmeta -i %s -d %s' %(histograms,histogramp2,histogramDir)
    
    #command
    cmd_all = ' ; '.join((mkIndexList,'\\',c18,'\\',c19,'\\',c20,'\\',c21,'\\',c22,'\\',c23,'\\',cleanup,cleanup2))
    p2=open('%s_p2command.sh'%(oezvP2),'w')
    p2.write('#!/bin/bash')
    p2.write('\n')
    p2.write('#BSUB -R %s'%(memf))
    p2.write('\n')
    p2.write('#BSUB -M %s'%(mem_MB*1000))
    p2.write('\n')
    p2.write('#BSUB -n %s'%(cpu))
    p2.write('\n')
    p2.write('#BSUB -o %s%s.out'%(od,on))
    p2.write('\n')         
    p2.write('#BSUB -J EZV2-%s'%(on))
    p2.write('date')
    p2.write('\n')              
    p2.write('%s'%(cmd_all))
    p2.write('\n')     
    p2.write('date')  
    p2.close()
            
    lsf_cmd = "bsub -q ezmeta -W 179:59 < %s_p2command.sh" %(oezvP2)
    print('==== Submitted LSF command for: %s ====' %(oezvP2))
    print(lsf_cmd + '\n')
    
    # submit the job
    os.system(lsf_cmd)
    
main()