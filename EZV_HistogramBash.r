#!/usr/bin/Rscript
# makes 2D histogram of depth of coverage along genome length using PBC.csv files
# label names are parsed from the file name

library(ggplot2)
library(plyr)
library(optparse)

option_list <- list(
   make_option(c("-d", "--directory"),
       help="csv file directory"))
       

opt = parse_args(OptionParser(option_list = option_list))

if (is.null(opt$directory)) {  
    warning('missing input directory [flag: -d]')
    q()
}

#set correct working directory
folder=opt$directory
setwd(folder)

args <- commandArgs(trailingOnly = TRUE)
PC = numeric() # percent covered
DC = numeric() # depth of coverage
FN = character() # file names
ALPHA = 0.6 # transparenecy for fill color
FN = list.files(pattern="*.csv", full.names=TRUE)

# create histograms for each mapping
for(file in FN){
if (file.size(file) == 0) next	
genome_name=file
data = read.table(genome_name, header=F, sep="\t")
colnames(data) <- c("genome","pos","coverage")
depth <- mean(data[,"coverage"])
this_genome = toString(data$genome[1])
print(this_genome)
### get how much of genome is covered ###
genome_len = max(data$pos)
naked = sum(data$coverage == 0) # get genome regions with no reads mapped
covered = genome_len - naked # get bases that are covered
percent_covered = (covered / genome_len) * 100   # % of genome covered at least once 

### get date and time and format nicely ###
t1 = format(Sys.Date(), format="%B %d, %Y")
t2 = format(Sys.time(), format="%I:%M %p")
name = strsplit(genome_name, ".csv")
cleanname = gsub("_", " ", name)
cleanname = gsub("./", "", cleanname)

#set PDF file with the clean name.
pdf(paste(cleanname,".pdf", sep=""), onefile=TRUE, width=10, height=5)
plot.title = paste("reads mapped to:", cleanname, sep=" ")
plot.subtitle = paste("genome: ",gsub("_", " ",this_genome),",", t1, "at", t2, sep=" ")
tit = bquote(atop(.(plot.title), atop(.(plot.subtitle), "")))

# axis labels
x.upper = paste("coverage: ", as.integer(percent_covered), "%", sep="")
x.lower = paste("genome length: ", genome_len, 
                " bp | average depth: ", as.integer(depth),
                " | tot covered bp: ", as.integer(covered), sep="")
xinfo = bquote(atop(.(x.upper), atop(italic(.(x.lower)), "")))

#replaced geom_area with geom_ribbon
#geom_ribbon vs geom_area: 2 seconds against 45 seconds to create the same exact graph!
data$coverage <- replace(data$coverage, length(data$coverage), 0) 
print(ggplot(data) + geom_ribbon(aes(data$pos,data$coverage,ymin=0,ymax=data$coverage), color="black", fill="#CC7A00", alpha=ALPHA)
    + labs(title=tit)
    + theme(plot.title=element_text(size=rel(1.2), colour="black")) 
    + xlab(xinfo) + theme(axis.title.x=element_text(size=rel(1.2)))
    + ylab("depth") + theme(axis.title.y=element_text(size=rel(1.2)))
    ) 

#print(ggplot(data) + geom_area(aes(data$pos,data$coverage), color="black", fill="#CC7A00", alpha=ALPHA)
#    + labs(title=tit)
#    + theme(plot.title=element_text(size=rel(1.2), colour="black")) 
#    + xlab(xinfo) + theme(axis.title.x=element_text(size=rel(1.2)))
#    + ylab("depth") + theme(axis.title.y=element_text(size=rel(1.2)))
#    ) 

dev.off()

}
