#!/usr/bin/python2.7

# 2018-Oct-25
# ezVIR_v2 : Full pipeline
# Tom J Petty & Francisco Brito - Zdobnov lab
# process a directory of fastq.gz paired-end reads
# "Simple" version of the pipeline, where the mapping against viral genomes is based on one index with all viral genomes present in the database.

import os
import sys
import argparse
import subprocess

## full path to executables ##

#software
trimmomatic  = '/your/path/to/trimmomatic'
tagdust      = '/your/path/to/tagdust'
samtools     = '/your/path/to/samtools'
snapMkIndex  = '/your/path/to/snap/snap-aligner index'
snap         = '/your/path/to/snap/snap-aligner'
bamToFastq   = '/your/path/to/bamToFastq'
bedtools     = '/your/path/to/bedtools'
rscript      = '/your/path/to/Rscript'
megahit      = '/your/path/to/megahit/megahit'
metaSpades   = '/your/path/to/spades/spades.py --meta'
IDBAmerge    = '/your/path/to/ĨDBA/fq2fa --merge --filter '
IDBA         = '/your/path/to/idba/bin/idba_ud'
blastN       = '/your/path/to/blast/blastn -task blastn'
diamond      = '/your/path/to/Diamond/diamond blastx'

#scripts and file links
ezvir2_p1    = '/your/path/to/ezvir2/EZVIR2_reports_phase-1.v150232.r'          #R script that generates reports
phase2       = '/your/path/to/ezvir2/phase2-moduleV19_BEE.py'                   #Python script that starts phase-2


# full path to databases and indexes
adapter_seqs = '/your/path/to/Trimmomatic-0.33/adapters/RNA or DNA adapters.fa'           # Path to Trimmomatic adapters (RNA or DNA, depending on the experiment)
sn_hgiEZ     = '/your/path/to/indexGenome'                                                # SNAP human genome index
sn_htiEZ     = '/your/path/to/indexTranscriptome'                                         # SNAP human transcriptome index
sn_virEZ     = '/your/path/to/ViralGenomes_140539'                                        # SNAP ezVIR database
ezv2_rf      = '/your/path/to/ezvir2/required_files'                                      # ezVIR2 required files
clusterFile  = '/your/path/to/ezvir2/required_files/ViralGenomes_140540_99.fasta.clstr'   # CDHit cluster file
genomeDB     = '/your/path/to/GENOMES_NO-ANIMAL-140540.ezv'                               # Viral database
uniVec       = '/your/path/to/univec Index'                                               # Index UniVec
diamondDB    = '/your/path/to/Diamond NR database'                                        # Diamond DB
blastDB      = '/your/path/to/Blast NT database'                                          # Blast nucleotide DB

def main():
    # get input info
    args = get_args()
    output_dir = os.path.abspath(args.output_dir[0])
    fastqs_dir = os.path.abspath(args.input_fastq[0])
    
    
    # prepare the output directories
    ods = {}
    od                = output_dir + '/'
    ods['log']        = od + 'log/'                   # Logs
    ods['trim']       = od + 'trim/'                  # trimmomatic data
    ods['dust']       = od + 'dust/'                  # complexity filtered reads 
    ods['map']        = od + 'map/'                   # mapping data (bam, sam, etc)
    ods['p1']         = od + 'phase-1/'               # phase-1 mapping (bam) and bedgraphs (bga)
    ods['p1_rep']     = od + 'phase-1/reports/'       # phase-1 diagnostic reports
    ods['assembly']   = od + 'assembly/'              # assembly data and statistics
    ods['megahit']    = od + 'assembly/megahit/'      # assembly data and statistics for Megahit    
    ods['metaspades'] = od + 'assembly/metaspades/'   # assembly data and statistics for MetaSpades  
    ods['IDBA']       = od + 'IDBA/'                  # assembly data and statistics for IDBA   
    ods['search']     = od + 'search/'                # homology search output
    ods['p2']         = od + 'phase-2/'               # phase-2 mapping (bam) and bedgraphs (bga)
    ods['p2_rep']     = od + 'phase-2/reports/'       # phase-2 diagnostic reports
    ods['p2log']      = od + 'phase-2/log/'           # phase-2 logs
    ods['p2hist']     = od + 'phase-2/histograms/'    # phase-2 histograms
    ods['blast']      = od + 'blast/'                 # assembly analysis
    ods['uniVec']     = od + 'uniVec/'                # uniVec alignment output
    
    # make the directories if they don't exist yet
    if not os.path.exists(od):
        os.makedirs(od)
        for key, val in sorted(ods.items()):
            os.makedirs(val)
    
    ods['main'] = od                            # add main directory to dictionary

    # get a listing of reads in the specified directory
    dirs = os.listdir(fastqs_dir)

    # create a dict of all the reads
    # for now, filter out the "_R2_" reads to get unique entries
    # key  = file name header, value = full path to reads
    fastq_files = {}    
    for fq_file in dirs:
        if "_R1_" in fq_file:
            # names are like :
            # T_5_GGACTCCT_L007_R1_001.fastq.gz
            # tmp_name[0] will be: T_5_GGACTCCT_L007
            tmp_name = fq_file.split('_R1_')
            output_name = tmp_name[0]
            
            f1 = fastqs_dir + '/' + fq_file
            f2 = fastqs_dir + '/' + tmp_name[0] + '_R2_' + tmp_name[1] # reconstruct the paired file name
            
            # store the file names and full path for this set of fastq
            fastq_files[output_name]= [f1, f2]


    # run the pipeline for each pair of fastq
    for fq in fastq_files:
        run_pipe(ods, fastq_files[fq][0], fastq_files[fq][1], fq, args.cpu[0], args.mem[0],args.start[0]) 
      
def run_pipe(od, f1, f2, on, cpu, mem,startingPoint):
    
    #set the correct path for the human transcriptome and genome indexes
    hgi=sn_hgiEZ
    hti=sn_htiEZ
    sn_vir= sn_virEZ

    ##----->>>>> COMMANDS: TRIM <<<<<-----##
    # here the raw reads are trimmed from adapters
    # input reads are gzipped, output from this step are unzipped reads 
    log_t = '%s%s.log' %(od['trim'], on)
    res_t = '%s%s.fq' %(od['trim'], on)
    
    #removes all adapters described on adapter_seqs. allows for 1 mismatch (:1:)
    #seeds are extended and clipped until the quality goes up to 30 for paired ends
    #10 for single ends. Runs max info trimming, target length of 40
    #strictness of 0.3 and the minimum length for a read to be kept is 36
    #UPDATE: changed the min length to 70.
    par_t = 'ILLUMINACLIP:%s:1:30:10:4:true MAXINFO:70:0.3 MINLEN:70' %(adapter_seqs)
 
    c1 = 'java -jar %s PE -threads %s -trimlog %s -basein %s -baseout %s %s ' %(trimmomatic, cpu, log_t, f1, res_t, par_t)
    #print('==== C1 ====')
    #print(c1 + '\n')
    
    ##------>>>> Trimmed Reads (unzipped) <<<<<------##
    f1_t   = '%s%s_1P.fq' %(od['trim'], on)
    f2_t   = '%s%s_2P.fq' %(od['trim'], on)
    
    #f1_t   = '%s%s_1P.clean.fq' %(od['trim'], on)
    #f2_t   = '%s%s_2P.clean.fq' %(od['trim'], on)
   
    ##----->>>>> COMMANDS: Cleanup <<<<<-----##
    # remove the RQ tags at the end of the reads, which are incompatible with downstream analysis software.
    cClean="sed -i -r 's/\/1;RQ:-1.00//' %s | sed -i -r 's/\/2;RQ:-1.00//' %s" %(f1_t,f2_t)
                
        
    ##----->>>>> COMMANDS: Map to Human <<<<<-----##
    #defines arguments for snap
    # overall map directory + output name for all files
    omap = ''.join((od['map'], on))
    
    # map on human     
    map_opts = '--hp -H 300000 -h 2000 -mcp 10000000'

    # SNAP mapping command
    c2 = '%s paired %s %s %s -t %s -o -bam %s.hg38.bam  ' %(snap, hgi, f1_t, f2_t, cpu, omap)
    # sort mapping and get unmapped (non-human reads)
    bam = '%s.hg38.bam' %(omap)
        
    # sort the bam on position
    c3 = '%s sort %s -o %s.hg38.sp.bam ' %(samtools, bam, omap)

    # merge temporary bam files and sort into read name order
    c7 = '%s view -@ %s -u -f 12 %s.hg38.sp.bam | %s sort -@ %s -n - -o %s.unmapped-hg38.bam' %(samtools, cpu, omap, samtools, cpu, omap)

    # extract the unmapped read pairs into their respective FASTQ files
    c8 = '%s -i %s.unmapped-hg38.bam -fq %s.unmapped-hg38_R1.fastq -fq2 %s.unmapped-hg38_R2.fastq' %(bamToFastq, omap, omap, omap)
    cleanup2=('rm -rf %s.hg38.bam' %(omap))

    ##------>>>> TNH: Trimmed & Non-Human Reads <<<<<------##
    f1_tnh  = '%s.unmapped-hg38_R1.fastq' %(omap)
    f2_tnh  = '%s.unmapped-hg38_R2.fastq' %(omap)
    
    #Second filtering pass, this time for the trasncriptome
    c2b = '%s paired %s %s %s -t %s -o -bam %s.hg38_gencode23.bam  ' %(snap, hti, f1_tnh, f2_tnh, cpu, omap)
    bam = '%s.hg38_gencode23.bam' %(omap)
    c3b = '%s sort -@ %s %s -o %s.hg38_gencode23.sp.bam ' %(samtools, cpu, bam, omap)
    c7b = '%s view -@ %s -u -f 12 %s.hg38_gencode23.sp.bam | %s sort -@ %s -n - -o %s.unmapped-hg38_gencode23.bam' %(samtools, cpu, omap, samtools, cpu, omap)
    c8b = '%s -i %s.unmapped-hg38_gencode23.bam -fq %s.unmapped-hg38_gencode23_R1.fastq -fq2 %s.unmapped-hg38_gencode23_R2.fastq' %(bamToFastq, omap, omap, omap)
    cleanup4=('rm -rf %s.hg38_gencode23.bam' %(omap))
     
    ## TNHT: Trimmed & Non-Human Transcriptome Reads ##
    f1_tnht  = '%s.unmapped-hg38_gencode23_R1.fastq' %(omap)
    f2_tnht  = '%s.unmapped-hg38_gencode23_R2.fastq' %(omap)
     
     
    #tagdust2 - Low-complexity filter
    o_dust  = '%s%s' %(od['dust'], on)
    c9 = '%s -t %s -dust 16 -1 R:N %s %s -o %s' %(tagdust,cpu,f1_tnht,f2_tnht,o_dust)

    ##TFNH: Trimmed, Filtered & Non-Human ##
    f1_tfnh  = '%s%s_READ1.fq' %(od['dust'], on)
    f2_tfnh  = '%s%s_READ2.fq' %(od['dust'], on)
    
    cClean2 = "sed -i -r 's/\/1;RQ:-1.00//' %s | sed -i -r 's/\/2;RQ:-1.00//' %s" %(f1_tfnh,f2_tfnh)

    #UniVec - Remove potential vectors and keep these reads for further investigation
    oVC = ''.join((od['uniVec'], on))
    cUV = '%s paired %s %s %s -t %s -o -bam %s.UniVec.bam  ' %(snap, uniVec, f1_tfnh, f2_tfnh, cpu, oVC)
    
    bam = '%s.UniVec.bam' %(oVC)
    cVCsort = '%s sort -@ %s %s -o %s.UniVec.sp.bam ' %(samtools, cpu, bam, oVC)
    cVCunm  = '%s view -@ %s -u -f 12 %s.UniVec.sp.bam | %s sort -@ %s -n - -o %s.unmapped-UniVec.bam' %(samtools, cpu, oVC, samtools, cpu, oVC)
    cVCs2f  = '%s -i %s.unmapped-UniVec.bam -fq %s.unmapped-UniVec_R1.fastq -fq2 %s.unmapped-UniVec_R2.fastq' %(bamToFastq, oVC, oVC, oVC)
    
    f1_tfnhu  = '%s%s.unmapped-UniVec_R1.fastq' %(od['uniVec'], on)
    f2_tfnhu  = '%s%s.unmapped-UniVec_R2.fastq' %(od['uniVec'], on)
    
    ##Phase-1##
    # overall ezVIR phase-1 directory + output name for all files
    oezv = ''.join((od['p1'], on))
    oezr = ''.join((od['p1_rep'], on))
    
    # SNAP mapping to ezVIR database
    map_opts = '-F a --hp -H 1000000 -h 10 -d 8'
    # SNAP mapping command
    c10 = '%s paired %s %s %s -t %s %s -o %s.ezv2.bam ' %(snap, sn_vir, f1_tfnhu, f2_tfnhu, cpu, map_opts, oezv)
    
    # sort the mapped reads by position
    c11 = '%s sort -@ %s %s.ezv2.bam -o %s.ezv2.sp.bam' %(samtools, cpu, oezv, oezv)
    
    # genome coverage command to create bga 
    c12 = '%s genomecov -ibam %s.ezv2.sp.bam -bga > %s.ezv2.bga' %(bedtools, oezv, oezv)
    
    # SNAP mapping output for report generation
    ezv2_in = '%s.ezv2.bga' %(oezv)
    
    # Statistics for report
    readSum= "%sSum" %(oezv)
    readNum= "%sNum" %(oezv)
    
    #calculate the average read length for R script option
    c13= "%s view -@ %s %s.ezv2.sp.bam | awk '{print length($10)}' | sort|awk '{s+=$1} END {print s}\' > %s" %(samtools, cpu, oezv,readSum)
    c14= "%s view -@ %s %s.ezv2.sp.bam | awk '{print length($10)}' | wc -l > %s" %(samtools, cpu, oezv,readNum)
   
    # report generation
    c15 = '%s %s -c %s/COLORS.ezv -v %s/%s -i %s -o %s -r %s -n %s' %(rscript, ezvir2_p1, ezv2_rf, ezv2_rf, genomeDB, ezv2_in, oezr, readSum, readNum)

    ##Phase-3##
    # assemble reads
    # assembly directory + output name for all files
    
    #MegaHit
    omega = ''.join((od['megahit'], on))
    oMH= '--min-count 1 --k-list 21,31,41,51,61,71,81,91,99 -t %s' %(cpu)
    cMH= "%s %s -1 %s -2 %s  -o %s" %(megahit, oMH, f1_tfnhu, f2_tfnhu, omega)
   
    #MetaSpades 
    #can't install on bee? ask fredrik.
    ometa=''.join((od['metaspades'], on))
    oMS='-t %s -m %s -k 21,31,41,51,61,71,81,91,99' %(cpu,mem)
    cMS='%s %s -1 %s -2 %s -o %s' %(metaSpades, oMS, f1_tfnhu, f2_tfnhu, ometa)
    
    #IDBA
    oIDBA=''.join((od['IDBA'], on))
    mIDBA= "%s %s %s %s.merged" %(IDBAmerge,f1_tfnhu,f2_tfnhu,oIDBA)
    optIDBA= "--pre_correction --num_threads 4 --mink 21 --maxk 100"
    aIDBA="%s %s -r %s.merged -o %s" %(IDBA, optIDBA ,oIDBA, oIDBA)
         
       
    ##----->>>>> Phase-2 - Strain Typing<<<<<-----##
    # set the necessary paths
    ezInput= ''.join((od['p1_rep'], on,'_ALL-RESULTS.ezv'))
    oezvP2 = ''.join((od['p2'], on))
    oezrP2 = str(od['p2'])
    
    c20 = 'python %s -ezIn %s -fq1 %s -fq2 %s -dir %s -o %s -c %s -mem %s -log %s -name %s -rs %s -rn %s' %(phase2, ezInput, clusterFile, f1_tfnh, f2_tfnh, oezvP2, oezrP2, cpu, mem, od['p2log'], on, readSum, readNum)
    
    #more cleanup steps; remove the trimmed and dusted files.
    cleanup5='rm -f %s/%s*' %(od['dust'],on)
    cleanup6='rm -f %s/%s*' %(od['trim'],on)
    cleanup7='rm -f %s/%s*' %(od['map'],on)
    cleanup8= 'rm -f %s/%s*.UniVec.bam' %(od['uniVec'],on)
    cleanup9='rm -f %s/%s*merged' %(od['IDBA'],on)
    log=''.join((od['log'], on,'.txt'))
    logC= ' 2>> %s' %(log)
    logS= '&>> %s' %(log)
    
    ##----->>>>> COMMANDS: Pipe and submit <<<<<-----##
    if startingPoint == 'all':
        cmd_all = ' ; '.join((c1+logC, cClean, c2+logS, c3+logC, c7+logC, c8+logC, c2b+logS, c3b+logC, c7b+logC, c8b+logC, c9+logC, cClean2, cUV+logS, cVCsort+logC, cVCunm+logC, cVCs2f+logC, c10+logS, c11+logC, c12+logC, c13+logC, c14+logC, c15+logC, cMH+logC, cMS+logC, mIDBA+logC, aIDBA+logC, c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
    if startingPoint == 'hmap':
        cmd_all = ' ; '.join((c2+logS, c3+logC, c7+logC, c8+logC, c2b+logS, c3b+logC, c7b+logC, c8b+logC, c9+logC, cClean2, cUV+logS, cVCsort+logC, cVCunm+logC, cVCs2f+logC, c10+logS, c11+logC, c12+logC, c13+logC, c14+logC, c15+logC, cMH+logC, cMS+logC, mIDBA+logC, aIDBA+logC, c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
    if startingPoint == 'dust':
        cmd_all = ' ; '.join((c9+logC, cClean2, cUV+logS, cVCsort+logC, cVCunm+logC, cVCs2f+logC, c10+logS, c11+logC, c12+logC, c13+logC, c14+logC, c15+logC, cMH+logC, cMS+logC, mIDBA+logC, aIDBA+logC, c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
    if startingPoint == 'vmap':
        cmd_all = ' ; '.join((c10+logS, c11+logC, c12+logC, c13+logC, c14+logC, c15+logC, cMH+logC, cMS+logC, mIDBA+logC, aIDBA+logC, c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
    if startingPoint == 'rprt':
        cmd_all = ' ; '.join((c13+logC, c14+logC, c15+logC, cMH+logC, cMS+logC, mIDBA+logC, aIDBA+logC, c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
    if startingPoint == 'asmbl':
        cmd_all = ' ; '.join((cMH+logC, cMS+logC, mIDBA+logC, aIDBA+logC, c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
    if startingPoint == 'p2':
        cmd_all = ' ; '.join((c20+logC, cleanup2+logC,cleanup4+logC,cleanup5+logC,cleanup6+logC,cleanup7,cleanup8,cleanup9))
        
    # build the LSF command
    # adjust memory values, -R is in MB
    mem_MB = int(mem) * 1000
    memf = 'rusage[mem=%s]' %(mem_MB)
    
    ## for lsf clusters: set max wall clock time - format hh:mm
    time = '59:59'
    
    # build command: lsf, slurm, or directly
    #remove the comment on the one you want
    #lsf_cmd = "bsub -q QUEUE_NAME -W %s -R %s -n %s -o %slog/%s.out -J EZV2-%s \"date ; %s ; date\"" %(time, memf, cpu, od['main'], on, on, cmd_all)
    #lsf_cmd = "sbatch -c %s -o %slog/%s.out -J EZV2_%s --wrap \"date ; %s ; date\"" %(cpu, od['main'], on, on, cmd_all)     
    #lsf_cmd = 'date; %s; date' %(cmd_all)
    print('==== Submitted  command for: %s ====' %(on))
    print(lsf_cmd + '\n') #prints the full command to check for errors
    
    # submit the job
    os.system(lsf_cmd)
        
#restricts number of CPUs (min 1, max 64)
def restricted_cpu(x):
    x = int(x)
    if x < 1 or x > 64:
        raise argparse.ArgumentTypeError("%r not in correct range of CPUs [1, 64]"%(x,))
    return x

# Command line arguments
# 1 - the path for the fastq input files
# 2 - the path for the output files
# 3 - the number of threads
# 4 - the amount of memory allocated
# 5 - where to start the pipeline
def get_args():
    parser = argparse.ArgumentParser(description='Sort and Filter fastq.gz')
    parser.add_argument('-i',  dest='input_fastq', type=str, nargs=1, required=True, help='dir of fastq.gz files')
    parser.add_argument('-od', dest='output_dir', type=str, nargs=1, required=True, help='Path to output directory')
    parser.add_argument('-c',  dest='cpu', type=restricted_cpu, nargs=1, required=True, help='Number of CPUs to use')
    parser.add_argument('-m',  dest='mem', type=str, nargs=1, required=True, help='Memory (in Gb)')
    parser.add_argument('-cl', dest='cluster', type=str, nargs=1, default='ezmeta', help='Cluster used for analysis: ezmeta or vit. Default: ezmeta')
    parser.add_argument('-st', dest='start',type=str, nargs=1, default='all', help='Choose where to start the pipeline: all - run everything || hmap - human mapping || dust - low complexity removal || vmap - viral mapping || rprt - phase 1 reports || asmbl - assembly || p2 - phase 2')
    #Get command line args
    return  parser.parse_args()

main()